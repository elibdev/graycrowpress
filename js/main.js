
// Fireup the plugins
//$(window).load(function() {
$(document).ready(function(){
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 120,
    itemMargin: 5,
    asNavFor: '#slider'
  });
   
  $('#slider').flexslider({
    animation: "fade",
    controlNav: false,
    animationLoop: false,
    slideshow: true,
    //itemWidth: 480,
    //itemMargin: 5,
    sync: "#carousel",
    start: function(slider){
      $('body').removeClass('loading');
    }
  });

	// initialise  slideshow
	$('.flexslider').flexslider({
    animation: "slide",
    slideshowSpeed: 7000,
    pauseOnAction: true,
    //itemWidth: 960,
    controlNav: "thumbnails",
    start: function(slider){
      $('body').removeClass('loading');
    }
  });

	// initialise  lightbox image zoom
	$('.fancybox').fancybox({
    //padding : 0,
    width : 1200,
    //fitToView : false,
    openEffect  : 'elastic'
  });

});

/**
 * Handles toggling the navigation menu for small screens.
 */
( function() {
	var button = document.getElementById( 'topnav' ).getElementsByTagName( 'div' )[0],
	    menu   = document.getElementById( 'topnav' ).getElementsByTagName( 'ul' )[0];

	if ( undefined === button )
		return false;

	// Hide button if menu is missing or empty.
	if ( undefined === menu || ! menu.childNodes.length ) {
		button.style.display = 'none';
		return false;
	}

	button.onclick = function() {
		if ( -1 == menu.className.indexOf( 'srt-menu' ) )
			menu.className = 'srt-menu';

		if ( -1 != button.className.indexOf( 'toggled-on' ) ) {
			button.className = button.className.replace( ' toggled-on', '' );
			menu.className = menu.className.replace( ' toggled-on', '' );
		} else {
			button.className += ' toggled-on';
			menu.className += ' toggled-on';
		}
	};
} )();
